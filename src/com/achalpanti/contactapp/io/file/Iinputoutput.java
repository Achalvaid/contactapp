/**
 * 
 */
package com.achalpanti.contactapp.io.file;

import com.achalpanti.contactapp.Contact;

/**
 * @author Achal
 *
 */
public interface Iinputoutput {
	void createConnection(String s) throws Exception;
	void save(Contact ref)throws Exception;
	void printAll()throws Exception;
	void closeConnection()throws Exception;

}
