/**
 * 
 */
package com.achalpanti.contactapp.io.file;

import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.achalpanti.contactapp.Contact;

/**
 * @author Achal
 *
 */
public class Serial implements Iinputoutput {

	/**
	 * 
	 */
	ObjectOutputStream ot;
	ObjectInputStream  it;
	
	public Serial() {
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.achalpanti.contactapp.io.file.Iinputoutput#createConnection(java.lang.String)
	 */
	@Override
	public void createConnection(String s) throws Exception {
		ot=new ObjectOutputStream(new FileOutputStream(s,true));

	}

	/* (non-Javadoc)
	 * @see com.achalpanti.contactapp.io.file.Iinputoutput#save(com.achalpanti.contactapp.Contact)
	 */
	@Override
	public void save(Contact ref) throws Exception {
			ot.writeObject(ref);
	}

	/* (non-Javadoc)
	 * @see com.achalpanti.contactapp.io.file.Iinputoutput#printAll()
	 */
	@Override
	public void printAll() throws Exception {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see com.achalpanti.contactapp.io.file.Iinputoutput#closeConnection()
	 */
	@Override
	public void closeConnection() throws Exception {
		// TODO Auto-generated method stub

	}

}
