/**
 * 
 */
package com.achalpanti.contactapp;

import java.io.Serializable;

/**
 * @author Achal
 *
 */
public class Contact implements Serializable {
	private String name;
	private String email;
	private long number;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getNumber() {
		return number;
	}

	public void setNumber(long number)throws Exception {
		if(number>= 19999999 && number<9999999999l)
		this.number = number;
		else
			throw new Exception("phone no out of range");
	}

}
